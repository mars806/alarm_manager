package com.hgh806.reminder.models;

public class SongModel {
    private long id;
    private String name;
    private String path;

    public SongModel(long id, String title, String path) {
        this.id = id;
        this.name = title;
        this.path = path;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return name;
    }

    public String getPath() {
        return path;
    }
}
