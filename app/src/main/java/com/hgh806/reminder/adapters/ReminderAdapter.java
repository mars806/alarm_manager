package com.hgh806.reminder.adapters;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.hgh806.reminder.R;
import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.ui.ReminderHandler;
import com.hgh806.reminder.ui.main.addReminder.AddReminderFragment;
import com.hgh806.reminder.ui.main.addReminder.ReminderOptionItemSelected;

import java.text.SimpleDateFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


/**
 * show list of reminders in homeFragment
 */


public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.MyViewHolder> {

    private ReminderOptionItemSelected listener;
    private List<ReminderModel> reminderModels;
    public AppCompatActivity context;

    public ReminderAdapter(List<ReminderModel> reminderModels, ReminderOptionItemSelected listener){
        this.reminderModels = reminderModels;
        this.listener = listener;
    }

    public void setItems(List<ReminderModel> reminderModels){
        this.reminderModels.clear();
        this.reminderModels.addAll(reminderModels);
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_reminder_item, parent, false);
        context = (AppCompatActivity) parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ReminderModel model = reminderModels.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return reminderModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txtTitle, txtDesc, txtDate;
        ImageView imgOptions, imgInfo;
        LottieAnimationView animationView;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDesc = itemView.findViewById(R.id.txtDesc);
            txtDate = itemView.findViewById(R.id.txtDate);
            imgOptions = itemView.findViewById(R.id.imgOptions);
            imgInfo = itemView.findViewById(R.id.imgReminder);
            animationView = itemView.findViewById(R.id.ltActivation);
        }

        void bind(ReminderModel model){
            ReminderHandler  reminderHandler = new ReminderHandler();
            txtDate.setText(reminderHandler.getTimeFormat(model.getAlarmTime())
                    + "   "
                    + reminderHandler.getDateFormat(model.getAlarmTime()));

            txtTitle.setText(model.getTitle());
            txtDesc.setText(model.getDescription());

            Uri path = Uri.parse(model.getImagePath());
            if (!path.toString().equals("")) {
                imgInfo.setImageURI(path);
                imgInfo.setScaleType(ImageView.ScaleType.FIT_XY);
            }else {
                imgInfo.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_add_a_photo_black_24dp));
                imgInfo.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }

            if (model.isActive())
                animationView.setVisibility(View.VISIBLE);
            else
                animationView.setVisibility(View.GONE);

            imgOptions.setOnClickListener(v -> listener.onOptionClick(model));

            itemView.setOnClickListener(v -> {

                Bundle args = new Bundle();
                args.putParcelable(AddReminderFragment.NAME, model);

                Navigation.findNavController(v).navigate(R.id.home_to_add, args);

            });
        }
    }
}
