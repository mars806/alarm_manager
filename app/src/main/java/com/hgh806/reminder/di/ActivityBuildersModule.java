package com.hgh806.reminder.di;

import com.hgh806.reminder.di.main.MainFragmentBuildersModule;
import com.hgh806.reminder.di.main.MainModule;
import com.hgh806.reminder.di.main.MainScope;
import com.hgh806.reminder.di.main.MainViewModelsModule;
import com.hgh806.reminder.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {

    @MainScope
    @ContributesAndroidInjector(modules = {MainViewModelsModule.class, MainFragmentBuildersModule.class,
            MainModule.class})
    abstract MainActivity contributeMainActivity();

}
