package com.hgh806.reminder.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.hgh806.reminder.service.AlarmService;
import com.hgh806.reminder.utils.Constants;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        int dayOfWeek = intent.getIntExtra(Constants.DAY_OF_WEEK, 0);
        long requestCode = intent.getLongExtra(Constants.REQUEST_ALARM_CODE, -1);

        Intent serviceIntent = new Intent(context, AlarmService.class);
        serviceIntent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        serviceIntent.putExtra(Constants.DAY_OF_WEEK, dayOfWeek);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(serviceIntent);
        } else {
            context.startService(serviceIntent);
        }
    }


}
