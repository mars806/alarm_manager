package com.hgh806.reminder.database;


import com.hgh806.reminder.models.AlarmDaysModel;
import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.ui.main.addReminder.ReminderDAO;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {ReminderModel.class, AlarmDaysModel.class}, version = 6, exportSchema = true)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ReminderDAO reminderDAO();

}
