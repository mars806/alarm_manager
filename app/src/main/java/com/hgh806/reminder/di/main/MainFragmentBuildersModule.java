package com.hgh806.reminder.di.main;

import com.hgh806.reminder.ui.main.addReminder.song.SongFragment;
import com.hgh806.reminder.ui.main.note.NotesFragment;
import com.hgh806.reminder.ui.main.addReminder.AddReminderFragment;
import com.hgh806.reminder.ui.main.home.HomeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract HomeFragment contributeHomeFragment();

    @ContributesAndroidInjector
    abstract NotesFragment contributeNotesFragment();

    @ContributesAndroidInjector
    abstract AddReminderFragment contributeAddReminderFragment();

    @ContributesAndroidInjector
    abstract SongFragment ContributeSongFragment();
}
