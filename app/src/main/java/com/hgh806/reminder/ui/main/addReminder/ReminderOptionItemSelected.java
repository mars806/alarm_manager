package com.hgh806.reminder.ui.main.addReminder;

import com.hgh806.reminder.models.ReminderModel;

public interface ReminderOptionItemSelected {
    void onOptionClick(ReminderModel model);
}
