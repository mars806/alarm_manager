package com.hgh806.reminder.di.main;

import android.app.AlarmManager;
import android.app.Application;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;

import com.hgh806.reminder.R;
import com.hgh806.reminder.adapters.SongsAdapter;
import com.hgh806.reminder.ui.main.MainActivity;
import com.hgh806.reminder.ui.main.addReminder.Repository;
import com.hgh806.reminder.utils.Constants;

import javax.inject.Named;

import androidx.core.app.NotificationCompat;
import androidx.room.PrimaryKey;
import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    @MainScope
    @Provides
    static SongsAdapter provideAdapter() {
        return new SongsAdapter();
    }


    @MainScope
    @Provides
    static ContentResolver provideContentResolver(Application application) {
        return application.getContentResolver();
    }

    @MainScope
    @Provides
    static Repository provideRepository(ContentResolver contentResolver, Application application) {
        return new Repository(contentResolver, application);
    }

    @MainScope
    @Provides
    static AlarmManager provideAlarmManager(Application application){
        return (AlarmManager) application.getSystemService(Context.ALARM_SERVICE);
    }

}

