package com.hgh806.reminder.ui.main.addReminder;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.hgh806.reminder.database.AppDatabase;
import com.hgh806.reminder.database.DatabaseClient;
import com.hgh806.reminder.models.AlarmDaysModel;
import com.hgh806.reminder.models.SongModel;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import timber.log.Timber;

public class Repository {

    private Context context;
    private ContentResolver contentResolver;
    private List<SongModel> songModels = new ArrayList<>();
    private MutableLiveData<SongModel> songModel = new MutableLiveData<>();
    private MutableLiveData<List<SongModel>> songs = new MutableLiveData<>();

    public Repository(ContentResolver contentResolver, Context context){
        this.contentResolver = contentResolver;
        this.context = context;
    }

    public MutableLiveData<List<SongModel>> getSongs(){

        Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(uri, null, null, null, null);
        if (cursor == null) {
            // query failed, handle error.
            Timber.i("cursor is null");
        } else if (!cursor.moveToFirst()) {
            // no media on the device
            Timber.i(" no media");
        } else {
            int titleColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
            int pathColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            do {
                long thisId = cursor.getLong(idColumn);
                String thisTitle = cursor.getString(titleColumn);
                String path = cursor.getString(pathColumn);
                songModels.add(new SongModel(thisId, thisTitle,path));

            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
        songs.setValue(songModels);
        return songs;
    }


    public MutableLiveData<SongModel> getSelectedSong(){
        return songModel;
    }

    public List<AlarmDaysModel> getDays(long currentTime) {
        AppDatabase database = DatabaseClient.getInstance(context).getAppDatabase();
        return database.reminderDAO().getDays(currentTime);
    }
}
