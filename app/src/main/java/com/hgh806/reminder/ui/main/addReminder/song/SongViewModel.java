package com.hgh806.reminder.ui.main.addReminder.song;

import android.media.AudioManager;
import android.media.MediaPlayer;

import com.hgh806.reminder.models.SongModel;
import com.hgh806.reminder.ui.main.addReminder.Repository;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import timber.log.Timber;

/**
 * choosing alarm song
 */

public class SongViewModel extends ViewModel {

    private MediaPlayer mediaPlayer;
    private Repository repository;

    @Inject
    public SongViewModel(Repository repository){
        this.repository = repository;
    }

    public MutableLiveData<List<SongModel>> getSongs(){
        return repository.getSongs();
    }

    public void playSong(SongModel model) {

        if (model == null){
            //Not selected song
            repository.getSelectedSong().postValue(null);
            return;
        }

        repository.getSelectedSong().setValue(model); //save song
        pauseSong(); // restart mediaPlayer

        try {
            String audioPath = model.getPath();
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(audioPath);
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
            Timber.e(e ,"playSong catch: ");
        }

    }

    public void pauseSong(){
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }

}
