package com.hgh806.reminder.di;

import android.app.Application;

import com.hgh806.reminder.database.AppDatabase;
import com.hgh806.reminder.database.DatabaseClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Singleton
    @Provides
    AppDatabase provideDatabase(Application application){
        return DatabaseClient.getInstance(application).getAppDatabase();
    }
}
