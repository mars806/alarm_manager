package com.hgh806.reminder.ui.main.home;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;

import com.hgh806.reminder.database.AppDatabase;
import com.hgh806.reminder.models.AlarmDaysModel;
import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.receiver.AlarmReceiver;
import com.hgh806.reminder.utils.Constants;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private AppDatabase database;
    private AlarmManager alarmManager;
    private Application application;
    @Inject
    public HomeViewModel(AppDatabase database, AlarmManager alarmManager, Application application){
        this.database = database;
        this.alarmManager = alarmManager;
        this.application = application;
    }

    List<ReminderModel> getReminders(){
        return database.reminderDAO().reminders();
    }

    public void deleteAlarm(ReminderModel model) {

        int repeatMode = model.getRepeatMode();
        switch (repeatMode) {
            case 0:
            case 1:
                cancelAlarm(0, model);
                database.reminderDAO().deleteReminder(model);
                break;
            case 2:
                List<AlarmDaysModel> daysModels = database.reminderDAO().getDays(model.getCurrentTime());
                for (AlarmDaysModel daysModel : daysModels){
                    cancelAlarm(daysModel.getDayOfWeek(), model);
                    database.reminderDAO().deleteDays(daysModel);
                }
                database.reminderDAO().deleteReminder(model);
                break;
        }
    }


    private void cancelAlarm(int day_of_week, ReminderModel model) {
        long requestCode = model.getCurrentTime() + day_of_week;
        Intent intent = new Intent(application, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                application, (int) requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }

    public void changeAlarmActivation(boolean isChecked, ReminderModel model) {
        if (isChecked) {
            addReminderToOS(model);
            database.reminderDAO().deActiveReminder(true, model.getCurrentTime());
        }else
            deActiveAlarm(model);
        }

    private void deActiveAlarm(ReminderModel model) {
        int repeatMode = model.getRepeatMode();
        switch (repeatMode) {
            case 0:
            case 1:
                cancelAlarm(0, model);
                database.reminderDAO().deActiveReminder(false, model.getCurrentTime());
                break;
            case 2:
                List<AlarmDaysModel> daysModels = database.reminderDAO().getDays(model.getCurrentTime());
                for (AlarmDaysModel daysModel : daysModels){
                    cancelAlarm(daysModel.getDayOfWeek(), model);
                }
                database.reminderDAO().deActiveReminder(false, model.getCurrentTime());
                break;
        }
    }

    private void addReminderToOS(ReminderModel model) {
        int repeatMode = model.getRepeatMode();
        switch (repeatMode) {
            case 0:
                startReminderNonRepeat(model);
                break;
            case 1:
                startReminderRepeatModeByTime(model); // like every 8 hour
                break;
            case 2:
                startReminderRepeatModeByDay(model); // like every monday
                break;
        }
    }

    private void startReminderRepeatModeByDay(ReminderModel model) {
        //get all selected days and set alarm for each
        long day_id = model.getCurrentTime();

        List<AlarmDaysModel> daysModels = database.reminderDAO().getDays(day_id);

        if (daysModels != null ){

            for (int i=0; i < daysModels.size(); i++){
                AlarmDaysModel daysModel = daysModels.get(i);
                forDay(daysModel.getDayOfWeek(), model);
            }
        }
    }


    private void forDay(int day_of_week, ReminderModel reminderModel){
        //because each alarm id most be unique use this formula to create it for each day
        long requestCode = reminderModel.getCurrentTime() + day_of_week;

        Intent intent = new Intent(application, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        intent.putExtra(Constants.DAY_OF_WEEK, day_of_week);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(application, (int) requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(reminderModel.getAlarmTime());
        calendar.set(Calendar.DAY_OF_WEEK, day_of_week);

        Calendar now = Calendar.getInstance();
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);

        if (calendar.before(now)) {    //this condition is used for future reminder that means your reminder not fire for past time
            calendar.add(Calendar.DATE, 7);
        }

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

    }


    private void startReminderRepeatModeByTime(ReminderModel model) {
        long requestCode = model.getCurrentTime();

        Intent intent = new Intent(application, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);

        Calendar calendar = Calendar.getInstance();
        long nowTime = calendar.getTimeInMillis();
        long alarmTime = model.getAlarmTime();

        while (nowTime > alarmTime){ //if alarm time be past, change it to tomorrow
            calendar.setTimeInMillis(alarmTime);
            calendar.add(Calendar.DATE, 1);//get tomorrow date
            alarmTime = calendar.getTimeInMillis();

            database.reminderDAO().updateReminderAlarmTime(alarmTime, requestCode);
        }


        PendingIntent alarmIntent = PendingIntent.getBroadcast(application, (int) requestCode, intent, 0);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, alarmIntent);

    }

    private void startReminderNonRepeat(ReminderModel model) {
        long requestCode = model.getCurrentTime();

        Calendar calendar = Calendar.getInstance();
        long nowTime = calendar.getTimeInMillis();
        long alarmTime = model.getAlarmTime();

        while (nowTime > alarmTime){ //if alarm time be past, change it to tomorrow
            calendar.setTimeInMillis(alarmTime);
            calendar.add(Calendar.DATE, 1);//get tomorrow date
            alarmTime = calendar.getTimeInMillis();

            database.reminderDAO().updateReminderAlarmTime(alarmTime, requestCode);
        }

        Intent intent = new Intent(application, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(application, (int) requestCode,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime , pendingIntent);

    }
}
