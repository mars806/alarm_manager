package com.hgh806.reminder.ui.main.home;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.hgh806.reminder.R;
import com.hgh806.reminder.adapters.ReminderAdapter;
import com.hgh806.reminder.databinding.FragmentHomeBinding;
import com.hgh806.reminder.databinding.HomeBottomSheetBinding;
import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.ui.main.MainActivity;
import com.hgh806.reminder.ui.main.addReminder.AddReminderFragment;
import com.hgh806.reminder.ui.main.addReminder.ReminderOptionItemSelected;
import com.hgh806.reminder.viewModels.ViewModelProviderFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.DaggerFragment;


public class HomeFragment extends DaggerFragment implements ReminderOptionItemSelected {


    private MainActivity activity;
    private AlertDialog alertDialog;
    private ReminderAdapter adapter;
    @Inject
    ViewModelProviderFactory providerFactory;
    private HomeViewModel viewModel;
    private FragmentHomeBinding binding;

    private HomeBottomSheetBinding bottomSheetBinding;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        binding = FragmentHomeBinding.bind(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MainActivity) getActivity();
        viewModel = new ViewModelProvider(this, providerFactory).get(HomeViewModel.class);

        initRecyclerView();
    }

    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new ReminderAdapter(viewModel.getReminders(), this);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onOptionClick(ReminderModel model) {
        View dialogView = getLayoutInflater().inflate(R.layout.home_bottom_sheet, null);

        bottomSheetBinding = HomeBottomSheetBinding.bind(dialogView);

        BottomSheetDialog dialog = new BottomSheetDialog(activity);
        dialog.setContentView(dialogView);
        dialog.create();
        dialog.show();

        bottomSheetBinding.mySwitch.setChecked(model.isActive());

        bottomSheetBinding.mySwitch.setOnCheckedChangeListener
                ((buttonView, isChecked) ->{
                    viewModel.changeAlarmActivation(isChecked, model);
                    adapter.setItems(viewModel.getReminders());
                });

        bottomSheetBinding.txtDelete.setOnClickListener(v -> {
            deleteAlarmDialog(model);
            dialog.dismiss();
        });

    }


    private void deleteAlarmDialog(ReminderModel model) {

        alertDialog = new AlertDialog.Builder(activity, R.style.DialogTheme)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setTitle("Are you sure you want to delete it?")
                .setPositiveButton(R.string.yes, (dialog, which) -> delete(model))
                .setNegativeButton(R.string.no, (dialog, which) -> dismissDialog())
                .create();

        alertDialog.show();
    }

    private void dismissDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    private void delete(ReminderModel model) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        viewModel.deleteAlarm(model);
        adapter.setItems(viewModel.getReminders()); // data set change
    }

}
