package com.hgh806.reminder.ui.main.addReminder;


import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;

import com.hgh806.reminder.database.AppDatabase;
import com.hgh806.reminder.models.AlarmDaysModel;
import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.receiver.AlarmReceiver;
import com.hgh806.reminder.utils.Constants;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 *
 */

public class AddReminderViewModel extends ViewModel {

    private Application application;
    private AppDatabase database;
    private AlarmManager alarmManager;

    private MutableLiveData<List<AlarmDaysModel>> days = new MutableLiveData<>();

    /**
     * @param application used for set alarm manager intent
     * @param database : save alarm details
     * @param alarmManager : set alarm
     */
    @Inject
    public AddReminderViewModel(Application application, AppDatabase database, AlarmManager alarmManager){
        this.application = application;
        this.database = database;
        this.alarmManager = alarmManager;
    }

    //get selected days
    MutableLiveData<List<AlarmDaysModel>> checkedDaysArray() {
        return days;
    }

    void initReminder(ReminderModel model){
        addReminderToDatabase(model);
        addReminderToOS(model);
    }

    private void addReminderToOS(ReminderModel model) {
        int repeatMode = model.getRepeatMode();
        switch (repeatMode) {
            case 0:
                startReminderNonRepeat(model);
                break;
            case 1:
                startReminderRepeatModeByTime(model); // like every 8 hour
                break;
            case 2:
                startReminderRepeatModeByDay(model); // like every monday
                break;
        }
    }

    private void startReminderRepeatModeByDay(ReminderModel model) {
        //get all selected days and set alarm for each
        long day_id = model.getCurrentTime();

        List<AlarmDaysModel> daysModels = database.reminderDAO().getDays(day_id);

        if (daysModels != null ){

            for (int i=0; i < daysModels.size(); i++){
                AlarmDaysModel daysModel = daysModels.get(i);
                forDay(daysModel.getDayOfWeek(), model);
            }
        }
    }


    private void forDay(int day_of_week, ReminderModel reminderModel){
        //because each alarm id most be unique use this formula to create it for each day
        long requestCode = reminderModel.getCurrentTime() + day_of_week;

        Intent intent = new Intent(application, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        intent.putExtra(Constants.DAY_OF_WEEK, day_of_week);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(application, (int) requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(reminderModel.getAlarmTime());
        calendar.set(Calendar.DAY_OF_WEEK, day_of_week);

        Calendar now = Calendar.getInstance();
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);

        if (calendar.before(now)) {    //this condition is used for future reminder that means your reminder not fire for past time
            calendar.add(Calendar.DATE, 7);
        }

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

        }


    private void startReminderRepeatModeByTime(ReminderModel model) {
        long requestCode = model.getCurrentTime();

        Intent intent = new Intent(application, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);

        Calendar calendar = Calendar.getInstance();
        long nowTime = calendar.getTimeInMillis();
        long alarmTime = model.getAlarmTime();

        while (nowTime > alarmTime){ //if alarm time be past, change it to tomorrow
            calendar.setTimeInMillis(alarmTime);
            calendar.add(Calendar.DATE, 1);//get tomorrow date
            alarmTime = calendar.getTimeInMillis();

            database.reminderDAO().updateReminderAlarmTime(alarmTime, requestCode);
        }


        PendingIntent alarmIntent = PendingIntent.getBroadcast(application, (int) requestCode, intent, 0);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, alarmIntent);

    }

    private void startReminderNonRepeat(ReminderModel model) {
        long requestCode = model.getCurrentTime();

        Calendar calendar = Calendar.getInstance();
        long nowTime = calendar.getTimeInMillis();
        long alarmTime = model.getAlarmTime();

        while (nowTime > alarmTime){ //if alarm time be past, change it to tomorrow
            calendar.setTimeInMillis(alarmTime);
            calendar.add(Calendar.DATE, 1);//get tomorrow date
            alarmTime = calendar.getTimeInMillis();

            database.reminderDAO().updateReminderAlarmTime(alarmTime, requestCode);
        }

        Intent intent = new Intent(application, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(application, (int) requestCode,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime , pendingIntent);

    }

    private void addReminderToDatabase(ReminderModel model) {
        if (model.getRepeatMode() == 2 && days.getValue() != null) {// repeatMode = 3 means repeat by days
            for (int i=0; i < days.getValue().size(); i++) {
                database.reminderDAO().insertDays(days.getValue().get(i));
            }
        }
        database.reminderDAO().insertReminder(model);
    }

}
