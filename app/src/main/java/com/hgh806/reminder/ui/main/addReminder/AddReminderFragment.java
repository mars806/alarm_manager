package com.hgh806.reminder.ui.main.addReminder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.hgh806.reminder.R;
import com.hgh806.reminder.adapters.SongsAdapter;
import com.hgh806.reminder.databinding.DialogChooseDaysBinding;
import com.hgh806.reminder.databinding.DialogSetRepeatTimeBinding;
import com.hgh806.reminder.databinding.FragmentAddReminderBinding;
import com.hgh806.reminder.models.AlarmDaysModel;
import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.models.SongModel;
import com.hgh806.reminder.ui.ReminderHandler;
import com.hgh806.reminder.ui.main.MainActivity;
import com.hgh806.reminder.ui.main.addReminder.song.SongFragment;
import com.hgh806.reminder.ui.main.home.HomeFragment;
import com.hgh806.reminder.utils.Constants;
import com.hgh806.reminder.viewModels.ViewModelProviderFactory;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import dagger.android.support.DaggerFragment;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;


public class AddReminderFragment extends DaggerFragment {

    private ReminderHandler reminderHandler = new ReminderHandler();
    private AddReminderViewModel viewModel;
    private MainActivity activity;
    private Calendar alarmId = Calendar.getInstance();
    private int checkedId;
    private AlertDialog alertDialogByTime;
    public static final String NAME = "reminder_model";
    private FragmentAddReminderBinding addReminderBinding;
    private DialogChooseDaysBinding chooseDaysBinding;
    private DialogSetRepeatTimeBinding repeatTimeBinding;
    private Calendar startCalendar = Calendar.getInstance();
    private int repeatMode = -1;
    private SongModel songModel;
    private String imagePath = "";
    private Long repeatTime = (long) -1;
    private BottomSheetDialog dialog;
    private View chooseDaysView, byTimeDialogView;
    private int[] checkBoxesId = new int[]{
            R.id.chMonday,
            R.id.chTuesday,
            R.id.chWednesday,
            R.id.chThursday,
            R.id.chFriday,
            R.id.chSaturday,
            R.id.chSunday
    };

    @Inject
    ViewModelProviderFactory providerFactory;

    @Inject
    SongsAdapter adapter;

    @Inject
    Repository repository;

    public AddReminderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true); // add menu in toolbar
        Timber.tag("AddReminder");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_reminder_menu, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_reminder, container, false);
        addReminderBinding = FragmentAddReminderBinding.bind(view);
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MainActivity) getActivity();
        viewModel = new ViewModelProvider(this, providerFactory).get(AddReminderViewModel.class);

        initViews();

        Bundle bundle = getArguments();
        if (bundle != null) {
            setHasOptionsMenu(false); // delete menu in toolbar

            ReminderModel model = bundle.getParcelable(NAME);
            if (model != null) {
                addReminderBinding.edtTitle.setText(model.getTitle());
                addReminderBinding.edtDesc.setText(model.getDescription());
                addReminderBinding.editClock.setText(reminderHandler.getTimeFormat(model.getAlarmTime()));
                addReminderBinding.image.setEnabled(false);
                addReminderBinding.txtSong.setEnabled(false);
                addReminderBinding.edtTitle.setEnabled(false);
                addReminderBinding.edtDesc.setEnabled(false);
                addReminderBinding.editClock.setEnabled(false);
                addReminderBinding.text2.setVisibility(View.GONE);
                addReminderBinding.radioGroup.setVisibility(View.GONE);


                if (!model.getSongName().equals(""))
                    addReminderBinding.txtSong.setText(model.getSongName());
                else
                    addReminderBinding.txtSong.setText("No Song");

                if (!model.getImagePath().equals(""))
                    addReminderBinding.image.setImageURI(Uri.parse(model.getImagePath()));

                repeatMode = model.getRepeatMode();
                switch (repeatMode) {
                    case 0:
                        showRepeatHolderView(R.id.noneRepeatHolder);
                        addReminderBinding.noneRepeatHolder.setText(getString(R.string.alarm_in) + reminderHandler.getDateFormat(model.getAlarmTime()));
                        break;
                    case 1:
                        showRepeatHolderView(R.id.time_holder);
                        int minutes = reminderHandler.getMinute(model.getRepeatTime());
                        int hours = reminderHandler.getHour(model.getRepeatTime());
                        addReminderBinding.timeHolder.edtHour.setText(String.valueOf(hours));
                        addReminderBinding.timeHolder.edtMinute.setText(String.valueOf(minutes));

                        break;
                    case 2:
                        showRepeatHolderView(R.id.day_holder);
                        List<AlarmDaysModel> days = repository.getDays(model.getCurrentTime());
                        for (AlarmDaysModel daysModel : days) {
                            int day = daysModel.getDayOfWeek();
                            CheckBox checkBox = view.findViewById(checkBoxesId[day]);
                            checkBox.setChecked(true);
                        }
                        break;
                }
            }
        }

        addReminderBinding.editClock.setOnClickListener(v -> openTimePickerDialog());
        addReminderBinding.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            this.checkedId = checkedId;
            setRepeatMode();
        });

        addReminderBinding.image.setOnClickListener(v -> onImageClick());
        addReminderBinding.txtSong.setOnClickListener(v -> chooseSongDialog());
        addReminderBinding.linear.setOnClickListener(v -> setRepeatMode());
        chooseDaysBinding.txtSubmit.setOnClickListener(v -> submitDays());
        repeatTimeBinding.txtSubmit.setOnClickListener(v ->{
            getRepeatTime();
            alertDialogByTime.dismiss();
        });
        repository.getSelectedSong().observe(getViewLifecycleOwner(), songObserver());
    }

    private Observer<? super SongModel> songObserver() {
        return (Observer<SongModel>) model -> {
            if (model != null) {
                songModel = model;
                addReminderBinding.txtSong.setText(model.getTitle());
            } else {
                addReminderBinding.txtSong.setText(R.string.alarm_song);
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.add) {
            addReminder();
        }
        return true;
    }

    private void addReminder() {
        /*
        first check all fields are entered
        create alarm model
        back to home
         */

        if (TextUtils.isEmpty(addReminderBinding.edtTitle.getText())) {
            addReminderBinding.edtTitle.setError("Set a title");
            return;
        }
        if (TextUtils.isEmpty(addReminderBinding.edtDesc.getText())) {
            addReminderBinding.edtDesc.setText("");
        }
        if (startCalendar == null) {
            addReminderBinding.editClock.setBackground(getResources().getDrawable(R.drawable.error_time_picker_border));
            return;
        }
        if (repeatMode == -1) {
            Toast.makeText(activity, "Choose your repeat mode", Toast.LENGTH_LONG).show();
            return;
        }
        if (songModel == null) {
            //if song be like this, media player alarm with default alarm song
            songModel = new SongModel(0, "", "");
        }

        ReminderModel reminderModel = new ReminderModel(0,
                alarmId.getTimeInMillis(),
                addReminderBinding.edtTitle.getText().toString(),
                addReminderBinding.edtDesc.getText().toString(),
                startCalendar.getTimeInMillis(),
                repeatMode,
                imagePath,
                repeatTime,
                songModel.getTitle(),
                songModel.getPath(),
                true
        );

        viewModel.initReminder(reminderModel);
        Navigation.findNavController(addReminderBinding.getRoot()).navigate(R.id.add_to_home);
    }

    private void setRepeatMode() {
        switch (checkedId) {
            case R.id.noneRepeat:
                repeatMode = 0;
                dialogNoneRepeatMode();
                showRepeatHolderView(R.id.noneRepeatHolder);
                break;
            case R.id.byTime:
                repeatMode = 1;
                dialogByTimeRepeatMode();
                showRepeatHolderView(R.id.time_holder);
                break;
            case R.id.byDay:
                repeatMode = 2;
                dialogByDayRepeatMode();
                showRepeatHolderView(R.id.day_holder);
                break;
        }
    }

    private void initViews(){
        chooseDaysView = getLayoutInflater().inflate(R.layout.dialog_choose_days,  null);
        chooseDaysBinding = DialogChooseDaysBinding.bind(chooseDaysView);

        byTimeDialogView = getLayoutInflater().inflate(R.layout.dialog_set_repeat_time, null);
        repeatTimeBinding = DialogSetRepeatTimeBinding.bind(byTimeDialogView);
    }

    private void dialogByDayRepeatMode(){
        if (chooseDaysView == null) {
            chooseDaysView = getLayoutInflater().inflate(R.layout.dialog_choose_days, null);
        } else {
            ViewGroup parent = (ViewGroup) chooseDaysView.getParent();
            if (parent != null) {
                parent.removeView(chooseDaysView);
            }
        }

        dialog = new BottomSheetDialog(activity);
        dialog.setContentView(chooseDaysView);
        dialog.setCancelable(false);
        dialog.create();
        dialog.show();
    }

    private void submitDays() {
        List<AlarmDaysModel> daysModelsList = getSelectedDayList();
        if (daysModelsList.size() == 0) {
            chooseDaysBinding.text.setText(R.string.select_atleast_one_day);
            chooseDaysBinding.text.setTextColor(Color.RED);
            return;
        }
        viewModel.checkedDaysArray().postValue(daysModelsList);
        dialog.dismiss();
    }

    private List<AlarmDaysModel> getSelectedDayList(){
        /*
        study all checkBoxes that is in the dialog
        if checked add to list
        and setCheck the same checkBox in the fragment
                 */
        List<AlarmDaysModel> daysModelsList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            CheckBox checkBox = addReminderBinding.dayHolder.getRoot().findViewById(checkBoxesId[i]);
            CheckBox dialogCheckBox = chooseDaysBinding.getRoot().findViewById(checkBoxesId[i]);
            if (dialogCheckBox.isChecked()) {
                daysModelsList.add(new AlarmDaysModel(0, alarmId.getTimeInMillis(), i));
                checkBox.setChecked(true);
            } else
                checkBox.setChecked(false);
        }
        return daysModelsList;
    }

    private void dialogByTimeRepeatMode() {
        if (byTimeDialogView == null) {
            byTimeDialogView = getLayoutInflater().inflate(R.layout.dialog_set_repeat_time, null);
        } else {
            ViewGroup parent = (ViewGroup) byTimeDialogView.getParent();
            if (parent != null) {
                parent.removeView(byTimeDialogView);
            }
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setView(byTimeDialogView);
        dialog.setCancelable(false);
        alertDialogByTime = dialog.show();
    }

    private void getRepeatTime() {
        int hour = 0, minute = 0;
        if (!TextUtils.isEmpty(repeatTimeBinding.edtHour.getText())) {
            hour = Integer.parseInt(repeatTimeBinding.edtHour.getText().toString());
        }
        if (!TextUtils.isEmpty(repeatTimeBinding.edtMinute.getText())) {
            minute = Integer.parseInt(repeatTimeBinding.edtMinute.getText().toString());
        }

        if (hour == 0 && minute == 0) {
            repeatTimeBinding.text.setText(R.string.enter_at_least_parameter);
            repeatTimeBinding.text.setTextColor(Color.RED);
            return;
        }

        repeatTime = reminderHandler.getTimeInMillis(hour, minute);

        addReminderBinding.timeHolder.edtHour.setText(String.valueOf(hour));
        addReminderBinding.timeHolder.edtMinute.setText(String.valueOf(minute));

    }

    private void dialogNoneRepeatMode() {
        /*
        myCalendar has now time and we add our hour and time
        startCalendar is main calendar that keep our time and date together
         */
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            startCalendar.set(Calendar.YEAR, year);
            startCalendar.set(Calendar.MONTH, monthOfYear);
            startCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String alarmDate = reminderHandler.getDateFormat(myCalendar.getTimeInMillis());
            addReminderBinding.noneRepeatHolder.setText(getString(R.string.alarm_in) + alarmDate);

        };

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                activity,
                R.style.DialogTheme,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setCancelable(false);
        datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, null, datePickerDialog);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);//disable past date
        datePickerDialog.show();

    }

    private void openTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        @SuppressLint("SetTextI18n")
        TimePickerDialog mTimePicker = new TimePickerDialog(
                getActivity(),
                R.style.DialogTheme,
                (timePicker, selectedHour, selectedMinute) ->
                {
                    addReminderBinding.editClock.setText(reminderHandler.checkDigit(selectedHour) +
                            ":" + reminderHandler.checkDigit(selectedMinute));
                    startCalendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                    startCalendar.set(Calendar.MINUTE, selectedMinute);
                }
                , hour, minute, true);

        mTimePicker.setTitle("Remind me at...");
        mTimePicker.show();
    }

    private void showRepeatHolderView(int viewHolderId) {
        addReminderBinding.getRoot().findViewById(R.id.time_holder).setVisibility(View.GONE);
        addReminderBinding.getRoot().findViewById(R.id.day_holder).setVisibility(View.GONE);
        addReminderBinding.getRoot().findViewById(R.id.noneRepeatHolder).setVisibility(View.GONE);

        addReminderBinding.getRoot().findViewById(viewHolderId).setVisibility(View.VISIBLE);
    }

    private void onImageClick() {
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            launchGalleryIntent();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            getWriteExternalPermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, Constants.REQUEST_IMAGE_CODE);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            final Uri imageUri = data.getData();
            if (imageUri != null)
                setupImageView(imageUri);
        } else {
            Toast.makeText(activity, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    private void setupImageView(Uri uri) {
        try {
            imagePath = uri.toString();
            final InputStream imageStream = activity.getContentResolver().openInputStream(uri);
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            addReminderBinding.image.setImageBitmap(selectedImage);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }

    private void chooseSongDialog() {
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            FragmentManager fm = activity.getSupportFragmentManager();
                            SongFragment fragment = new SongFragment(activity, adapter);
                            fragment.setCancelable(false);
                            fragment.show(fm, "song_fragment");

                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            getWriteExternalPermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }

    private void getWriteExternalPermission() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1052);
    }

}
