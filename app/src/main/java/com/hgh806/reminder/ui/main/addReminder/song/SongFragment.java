package com.hgh806.reminder.ui.main.addReminder.song;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hgh806.reminder.R;
import com.hgh806.reminder.adapters.SongsAdapter;
import com.hgh806.reminder.databinding.FragementChooseSongBinding;
import com.hgh806.reminder.viewModels.ViewModelProviderFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.DaggerDialogFragment;

public class SongFragment extends DaggerDialogFragment implements android.view.View.OnClickListener {

    private Context context;
    private FragementChooseSongBinding binding;

    private SongsAdapter adapter;
    private SongViewModel viewModel;

    @Inject
    ViewModelProviderFactory providerFactory;

    public SongFragment(@NonNull Context context, SongsAdapter adapter) {
        this.context = context;
        this.adapter = adapter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragement_choose_song, container, false);
        binding = FragementChooseSongBinding.bind(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this, providerFactory).get(SongViewModel.class);

        initRecyclerView();

        binding.btnOk.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);

    }

    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setAdapter(adapter);

        adapter.setPosts(viewModel.getSongs(), viewModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOk:
                viewModel.pauseSong();
                dismiss();
                break;
            case R.id.btnCancel:
                viewModel.pauseSong();
                viewModel.playSong(null);
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}