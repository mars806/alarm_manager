package com.hgh806.reminder.utils;

public class Constants {

    public static final int REQUEST_IMAGE_CODE = 101;
    public static final int REQUEST_NOTIFY_CODE = 201;
    public static final String REQUEST_ALARM_CODE = "requestCode";
    public static final String DAY_OF_WEEK = "dayOfWeek";
    public static final String CLOSE = "close";
    public static final String SNOOZE = "snooze";


}
