package com.hgh806.reminder.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "alarm_days")
public class AlarmDaysModel {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "day_id")
    private Long dayId;
    @ColumnInfo(name = "day")
    private int dayOfWeek;

    public AlarmDaysModel(int id, Long dayId, int dayOfWeek) {
        this.id = id;
        this.dayId = dayId;
        this.dayOfWeek = dayOfWeek;
    }

    public int getId() {
        return id;
    }

    public Long getDayId() {
        return dayId;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }
}
