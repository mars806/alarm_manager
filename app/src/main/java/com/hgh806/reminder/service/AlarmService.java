package com.hgh806.reminder.service;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;

import com.hgh806.reminder.R;
import com.hgh806.reminder.database.AppDatabase;
import com.hgh806.reminder.database.DatabaseClient;
import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.receiver.AlarmReceiver;
import com.hgh806.reminder.ui.main.MainActivity;
import com.hgh806.reminder.utils.Constants;

import java.io.IOException;
import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import timber.log.Timber;


/**
 *this service called when receiver receive alarm
 *and keep alarming until user stop it with close button on notification
 */

public class AlarmService extends Service {

    private CountDownTimer timer;
    AppDatabase database;
    AlarmManager alarmManager;
    MediaPlayer mediaPlayer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.tag("AddReminderService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent.getAction() != null) {

            if (intent.getAction().equals(Constants.CLOSE))
                stopAlarming();
            else if (intent.getAction().equals(Constants.SNOOZE)) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer = null;
            }

        }else {

            alarmManager = (AlarmManager) this.getSystemService(ALARM_SERVICE);

            int dayOfWeek = intent.getIntExtra(Constants.DAY_OF_WEEK, 0);
            long requestCode = intent.getLongExtra(Constants.REQUEST_ALARM_CODE, -1) - dayOfWeek;

            database = DatabaseClient.getInstance(this).getAppDatabase();
            ReminderModel model = database.reminderDAO().getModel(requestCode);

            startForeground(1, provideNotification(this, model).build());

            startTimer(true, model);

            playSong(model);

            initReminder(model, dayOfWeek);
        }
        return START_STICKY;
    }

    public void startTimer(boolean start, ReminderModel model) {
        //this timer repeat alarming every 10 min until user pause this service from notification
        if (timer != null) {
            timer.cancel();
        }

        if (start) {
            timer = new CountDownTimer(10 * 60 * 1000, 1000) {

                public void onTick(long millisUntilFinished) {
                    //nothing to do
                }

                public void onFinish() {
                    playSong(model);
                    provideNotification(getApplicationContext(), model);
                    startTimer(true, model);
                }
            }.start();

        } else if (timer != null)
            timer.cancel();
    }


    private void initReminder(ReminderModel model, int dayOfWeek) {
        switch (model.getRepeatMode()) {
            case 0:
                deActiveInDatabase(model);
                break;
            case 1: //by Time Repeat
                setRepeatingAlarmByTime(model);
                break;
            case 2: //by day Repeat
                setAlarmByDay(dayOfWeek, model);
                break;
        }
    }

    private void deActiveInDatabase(ReminderModel model) {
        database.reminderDAO().deActiveReminder(false, model.getCurrentTime());
    }

    private void playSong(ReminderModel model) {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer = null;
            }

            String audioPath = model.getSongPath();
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(audioPath);
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {

            Uri notification2 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification2);
            r.play();

            e.printStackTrace();
            Timber.e(e, e.toString());
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "reminder";
            String description = "show notification for your reminders";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("reminder", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

    }

    private NotificationCompat.Builder provideNotification(Context context, ReminderModel model) {
        createNotificationChannel();

        Intent stopIntent = new Intent(this, AlarmService.class);
        stopIntent.setAction(Constants.CLOSE);
        PendingIntent stopPendingIntent = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Intent snoozeIntent = new Intent(this, AlarmService.class);
        snoozeIntent.setAction(Constants.SNOOZE);
        PendingIntent snoozePendingIntent = PendingIntent.getService(this, 1, snoozeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, Constants.REQUEST_NOTIFY_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Builder(context, "reminder")
                .setContentTitle(model.getTitle())
                .setContentText("Click on notification to see reminder")
                .setSmallIcon(R.drawable.splash)
                .addAction(R.drawable.ic_close_black_24dp, "Close", stopPendingIntent)
                .addAction(R.drawable.ic_update_black_24dp, "Snooze", snoozePendingIntent)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle());
    }

    private void setRepeatingAlarmByTime(ReminderModel model) {
        /*
        we get repeat time and create new alarm for it
        on old requestCode that was in database
         */
        Calendar calendar = Calendar.getInstance();
        long currentTime = calendar.getTimeInMillis();
        long alarmTime = currentTime + model.getRepeatTime();

        long requestCode = model.getCurrentTime();
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, (int) requestCode, intent, 0);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, alarmIntent);

    }

    private void setAlarmByDay(int day_of_week, ReminderModel reminderModel) {
        //Obtain requestCode for each day of an alarm
        long requestCode = reminderModel.getCurrentTime() + day_of_week;

        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        intent.putExtra(Constants.DAY_OF_WEEK, day_of_week);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, (int) requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(reminderModel.getAlarmTime());
        //set date for next week
        calendar.add(Calendar.DATE, 7);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

        updateDataBase(calendar.getTimeInMillis(), reminderModel.getCurrentTime());

    }

    private void updateDataBase(long alarmTime, long alarmId) {
        //update database because we need alarm time for set new repeat time on next date (@setAlarmByDay)
        database.reminderDAO().updateReminderAlarmTime(alarmTime, alarmId);
    }

    private void stopAlarming() {
        timer.cancel();
        mediaPlayer.stop();
        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }
}