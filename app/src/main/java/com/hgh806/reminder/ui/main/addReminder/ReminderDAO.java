package com.hgh806.reminder.ui.main.addReminder;

import com.hgh806.reminder.models.AlarmDaysModel;
import com.hgh806.reminder.models.ReminderModel;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


@Dao
public interface ReminderDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDays(AlarmDaysModel model);

    @Insert
    void insertReminder(ReminderModel... users);

    @Query("SELECT * FROM reminder")
    List<ReminderModel> reminders();

    @Query("SELECT * FROM reminder WHERE `current_time` = :currentTime")
    ReminderModel getModel(Long currentTime);

    @Query("SELECT * FROM alarm_days WHERE day_id = :currentTime")
    List<AlarmDaysModel> getDays(Long currentTime);

    @Query("UPDATE reminder SET alarm_time = :alarmTime WHERE `current_time` = :alarmId")
    void updateReminderAlarmTime(long alarmTime, long alarmId);

    @Delete
    void deleteReminder(ReminderModel...model);

    @Delete
    void deleteDays(AlarmDaysModel...model);

    @Query("UPDATE reminder SET is_active = :isActive WHERE `current_time` = :alarmId")
    void deActiveReminder(boolean isActive, long alarmId);

}
