package com.hgh806.reminder.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hgh806.reminder.R;
import com.hgh806.reminder.models.SongModel;
import com.hgh806.reminder.ui.main.addReminder.song.SongViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

/**
 *show list of songs in internal storage
 * onClick method of items passed to the fragment
 */


public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.MyViewHolder> {

    private List<SongModel> songModelList;
    private SongViewModel viewModel;
    private int lastSelectedSong = -1;
    private int reselect = -1;


    /**
     * @param songModelList is list of songs
     * @param viewModel used for play and pause methods
     *
     * after generate list, call notifyDataSetChanged() to apply changes
     */
    public void setPosts(LiveData<List<SongModel>> songModelList, SongViewModel viewModel) {
        this.songModelList = songModelList.getValue();
        this.viewModel = viewModel;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_song_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(songModelList.get(position));

        /*
         by first click on a song -> play
         by second click on that song -> pause
         by third click on that song -> replay ----- @param reselect used for this
         */
        holder.container.setOnClickListener(v -> {
            if (position == lastSelectedSong && lastSelectedSong == reselect) { // is playing
                viewModel.pauseSong();
            } else {
                SongModel model = songModelList.get(position);
                viewModel.playSong(model);
            }

            lastSelectedSong = position;

            //reset the color of all items and change color of new selected item
            notifyDataSetChanged();
        });

        //change icon and color of last Selected Song
        if (position == lastSelectedSong) {
            holder.container.setBackgroundColor(R.drawable.submit_border);

            if (lastSelectedSong == reselect) { // now is playing
                reselect = -1;
                holder.imageView.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
            } else { // now in not playing
                holder.imageView.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
                reselect = lastSelectedSong;
            }

        } else { //default color and icon of items
            holder.imageView.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
            holder.container.setBackgroundColor(Color.TRANSPARENT);
        }
    }
    @Override
    public int getItemCount() {
        return songModelList.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        ImageView imageView;
        ConstraintLayout container;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            imageView = itemView.findViewById(R.id.imgPlay);
            container = itemView.findViewById(R.id.constraint_layout);
        }

        void bind(SongModel model){
            txtTitle.setText(model.getTitle());
        }

    }
}
