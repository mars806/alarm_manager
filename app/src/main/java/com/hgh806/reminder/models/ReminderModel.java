package com.hgh806.reminder.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "reminder")
public class ReminderModel implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "current_time")
    private Long currentTime;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "alarm_time")
    private Long alarmTime;

    @ColumnInfo(name = "repeat_mode")
    private Integer repeatMode;

    @ColumnInfo(name = "image_path")
    private String imagePath;

    @ColumnInfo(name = "repeat_time")
    private Long repeatTime;

    @ColumnInfo(name = "song_name")
    private String songName;

    @ColumnInfo(name = "song_path")
    private String songPath;

    @ColumnInfo(name = "is_active")
    private boolean isActive;

    public ReminderModel(Integer id, Long currentTime, String title, String description, Long alarmTime,
                         Integer repeatMode, String imagePath, Long repeatTime,
                         String songName, String songPath, boolean isActive) {
        this.id = id;
        this.currentTime = currentTime;
        this.title = title;
        this.description = description;
        this.alarmTime = alarmTime;
        this.repeatMode = repeatMode;
        this.imagePath = imagePath;
        this.repeatTime = repeatTime;
        this.songName = songName;
        this.songPath = songPath;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Long getCurrentTime() {
        return currentTime;
    }

    public String getDescription() {
        return description;
    }

    public Long getAlarmTime() {
        return alarmTime;
    }

    public Integer getRepeatMode() {
        return repeatMode;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Long getRepeatTime() {
        return repeatTime;
    }

    public String getSongName() {
        return songName;
    }

    public String getSongPath() {
        return songPath;
    }

    public boolean isActive() {
        return isActive;
    }


    protected ReminderModel(Parcel in) {
        id = in.readInt();
        currentTime = in.readByte() == 0x00 ? null : in.readLong();
        title = in.readString();
        description = in.readString();
        alarmTime = in.readByte() == 0x00 ? null : in.readLong();
        repeatMode = in.readByte() == 0x00 ? null : in.readInt();
        imagePath = in.readString();
        repeatTime = in.readByte() == 0x00 ? null : in.readLong();
        songName = in.readString();
        songPath = in.readString();
        isActive = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        if (currentTime == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(currentTime);
        }
        dest.writeString(title);
        dest.writeString(description);
        if (alarmTime == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(alarmTime);
        }
        if (repeatMode == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(repeatMode);
        }
        dest.writeString(imagePath);
        if (repeatTime == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(repeatTime);
        }
        dest.writeString(songName);
        dest.writeString(songPath);
        dest.writeByte((byte) (isActive ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ReminderModel> CREATOR = new Parcelable.Creator<ReminderModel>() {
        @Override
        public ReminderModel createFromParcel(Parcel in) {
            return new ReminderModel(in);
        }

        @Override
        public ReminderModel[] newArray(int size) {
            return new ReminderModel[size];
        }
    };
}
