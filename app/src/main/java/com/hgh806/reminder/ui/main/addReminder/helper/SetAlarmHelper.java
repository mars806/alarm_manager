package com.hgh806.reminder.ui.main.addReminder.helper;


import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;

import com.hgh806.reminder.models.ReminderModel;
import com.hgh806.reminder.receiver.AlarmReceiver;
import com.hgh806.reminder.utils.Constants;

import java.util.Calendar;

public class SetAlarmHelper {

    private Application application;
    private AlarmManager alarmManager;

    public SetAlarmHelper(Application application, AlarmManager alarmManager) {
        this.application = application;
        this.alarmManager = alarmManager;
    }

    private void setAlarmByDay(String dayOfWeek, ReminderModel reminderModel) {

        switch (dayOfWeek) {
            case "Monday":
                forDay(2, reminderModel);
                break;
            case "Tuesday":
                forDay(3, reminderModel);
                break;
            case "Wednesday":
                forDay(4, reminderModel);
                break;
            case "Thursday":
                forDay(5, reminderModel);
                break;
            case "Friday":
                forDay(6, reminderModel);
                break;
            case "Saturday":
                forDay(7, reminderModel);
                break;
            case "sunday":
                forDay(1, reminderModel);
                break;
        }
    }

    private void forDay(int day_of_week, ReminderModel reminderModel){
        //because each alarm id most be unique use this formula to create it for each day
        long requestCode = reminderModel.getCurrentTime() + day_of_week;

        Intent intent = new Intent(application, AlarmReceiver.class);
        intent.putExtra(Constants.REQUEST_ALARM_CODE, requestCode);
        intent.putExtra(Constants.DAY_OF_WEEK, day_of_week);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(application, (int) requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(reminderModel.getAlarmTime());
        calendar.set(Calendar.DAY_OF_WEEK, day_of_week);

        Calendar now = Calendar.getInstance();
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);

        if (calendar.before(now)) {    //this condition is used for future reminder that means your reminder not fire for past time
            calendar.add(Calendar.DATE, 7);
        }

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

    }



    public void setAlarmByDate(){

    }

    public void setAlarmByTime(){

    }

}
