package com.hgh806.reminder.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ReminderHandler {

    public int getMinute(long time){
        return (int) ((time / (1000 * 60)) % 60);
    }

    public int getHour(long time){
        return (int) ((time / (1000 * 60 * 60)) % 24);
    }

    public String getDateFormat(long time){
        String myFormat = "yyyy/MM/dd";
        DateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        return sdf.format(time);
    }

    public String getTimeFormat(long time){
        String timeFormat = "HH:mm";
        DateFormat formatter = new SimpleDateFormat(timeFormat, Locale.getDefault());
        return formatter.format(time);
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    public long getTimeInMillis(int hour, int minute){
        return hour * 3600000 + minute * 60000;
    }
}
