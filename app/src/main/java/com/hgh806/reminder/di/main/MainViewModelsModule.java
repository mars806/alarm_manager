package com.hgh806.reminder.di.main;

import com.hgh806.reminder.di.ViewModelKey;
import com.hgh806.reminder.ui.main.addReminder.song.SongViewModel;
import com.hgh806.reminder.ui.main.note.NotesViewModel;
import com.hgh806.reminder.ui.main.addReminder.AddReminderViewModel;
import com.hgh806.reminder.ui.main.home.HomeViewModel;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    public abstract ViewModel bindHomeViewModel(HomeViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NotesViewModel.class)
    public abstract ViewModel bindNotesViewModel(NotesViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddReminderViewModel.class)
    public abstract ViewModel bindAddReminderViewModel(AddReminderViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SongViewModel.class)
    public abstract ViewModel bindSongViewModel(SongViewModel viewModel);
}


